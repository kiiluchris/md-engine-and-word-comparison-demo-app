import tippy, { followCursor } from "tippy.js";
import "tippy.js/dist/tippy.css"

interface TooltipParams {
  content?: string;
  followCursor?: boolean;
  triggerTarget?: Element | null;
}

export default function tooltip(node: HTMLElement, params: TooltipParams = {}) {
  const title = node.title;
  const label = node.getAttribute("aria-label");
  const content = params.content || title || label;

  if (!label) node.setAttribute("aria-label", content);

  node.title = "";
  const plugins = [followCursor]

  const tip = tippy(node, { content, plugins, ...params });

  return {
    update: (newParams: TooltipParams) => tip.setProps({
      content, plugins, ...newParams
    }),
    destroy: () => tip.destroy(),
  };
};
