export function trace<T>(obj: T): T {
  console.log(JSON.stringify(obj, null, '  '))
  return obj
}
