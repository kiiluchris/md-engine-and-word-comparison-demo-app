import { createEventDispatcher } from "svelte";

export function alertDispatcher() {
  const dispatch = createEventDispatcher()
  return {
    dispatchSuccessMessage(message: string) {
      return dispatch("success", {
        message,
      })
    },
    dispatchErrorMessage(message: string) {
      return dispatch("error", {
        message,
      })
    }
  }
}


