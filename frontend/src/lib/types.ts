export type Page = {
  page_id: number;
  url: string;
  version: number;
}

export type WordCount = {
  text: string;
  count: number;
}

export type AlertMessage = {
  message: string;
}

export type SvelecteOption<T> = {
  label: string;
  value: T;
}

export type NumberOption = SvelecteOption<number>;

type WordCountMapWithPage = {
  page: Page;
  word_occurrences: {
    [text: string]: number;
  }
}

type PageWordCountMap = {
  [page: number]: WordCount[];
}

export type PageComparisonData = {
  common: PageWordCountMap;
  unique: PageWordCountMap;
  all_occurrences: WordCountMapWithPage[];
}
