import type { Page, PageComparisonData, WordCount } from './types'


type ErrorMessage = string
type PromiseWithError<T> = Promise<[T | null, ErrorMessage]>

const wrapPromiseError = <T>(promise: Promise<T>, errorMessage?: string): PromiseWithError<T> => {
  return promise.then(
    result => [result, ""],
    err => [null, errorMessage || err.message],
  )
}

const myFetch = async <T>(
  urlPath: string,
  requestErrorMessage: string,
  requestParseErrorMessage: string,
  init: RequestInit = {},
): Promise<[T | null, ErrorMessage]> => {
  const [response, fetchError] = await wrapPromiseError(fetch(`/api${urlPath}`, {
    ...init,
    headers: {
      ...init.headers,
      "Content-Type": "application/json",
      "Accept": "application/json",
    }
  }), requestErrorMessage)
  if (fetchError) {
    return [null, fetchError]
  }
  return await wrapPromiseError(response.json(), requestParseErrorMessage)
}


export function fetchPages(): PromiseWithError<Page[]> {
  return myFetch(
    "/page", "Failed to refresh pages from server",
    "Failed to decode fetched pages",
  )
}

export function savePage(data: { url: string }): PromiseWithError<{ message: string }> {
  return myFetch("/page", `Failed to fetch and save page at URL ${data.url}`, "", {
    method: "POST",
    body: JSON.stringify(data)
  })
}

export function fetchWordCounts(pageId: string): PromiseWithError<WordCount[]> {
  return myFetch(
    `/word-counts?id=${pageId}`,
    "Failed to fetch word counts of selected page",
    "Failed to decode fetched word occurrence counts",
  );
}

export function fetchPageComparisons(pageIds: number[]): PromiseWithError<PageComparisonData> {
  const ids = pageIds.map((v) => v.toString()).join(",");
  return myFetch(
    `/page/compare?pages=${ids}`,
    "Failed to fetch page comparison data",
    "Failed to decode fetched page comparison data",
  )
}
