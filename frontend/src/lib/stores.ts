import { writable } from 'svelte/store'

const makeAlertStore = () => {
  const { subscribe, set } = writable({
    message: "",
    hidden: true,
  })

  const show = (message: string) => set({
    message,
    hidden: false,
  })

  return {
    subscribe,
    show: show,
    showHandler: (e: CustomEvent<{ message: string }>) => {
      show(e.detail.message)
    },
    hide: () => set({
      message: "",
      hidden: true,
    }),
  }
}

export const successAlert = makeAlertStore()
export const errorAlert = makeAlertStore()


