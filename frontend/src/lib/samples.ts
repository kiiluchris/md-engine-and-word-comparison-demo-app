export const sampleMarkdown = `\
# Intro
Go ahead, play around with the editor!
Be sure to check out ***bold-italic***, **bold** and *italic* styling, or even [links](https://google.com).
You can type the Markdown syntax, use the toolbar, or use shortcuts like \`cmd-b\` or \`ctrl-b\`.

## Lists
Unordered lists can be started using the toolbar or by typing \`* \`, \`- \`, or \`+ \`. Ordered lists can be started by typing \`1. \`.

#### Unordered
* Lists are a piece of cake
* They even auto continue as you type
* A double enter will end them
* Tabs and shift-tabs work too

#### Ordered
1. Numbered lists...
    1. Child
2. ...work too!
    * Another child


## What about images?
![Yes](https://i.imgur.com/sZlktY7.png)

## BlockQuotes

> A blockquote

> Another blockquote
>> A nested blockquote
>>> A double nested blockquote
>> Back to level two
> Back to level one

## Code Block

      <div>
        <p>A paragraph</p>
      </div>


      A line
      Another line
      A third line



*Sample text taken from demo of [SimpleMDE Markdown Editor](https://simplemde.com/)*

`;
