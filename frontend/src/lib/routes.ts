import MarkdownR from '$routes/markdown.svelte'
import WordCountR from "$routes/word-counts.svelte"
import NotFoundR from '$routes/404.svelte'

type Route = {
  name: string;
  component: any;
  lang?: {
    [language: string]: string;
  };
  nestedRoutes?: Route[];
}
export const routes: Route[] = [
  {
    name: "/",
    component: MarkdownR,
  },
  {
    name: "/word-counts",
    component: WordCountR,
  },
  {
    name: "404",
    component: NotFoundR,
  }
]
