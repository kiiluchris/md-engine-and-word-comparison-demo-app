import svelte from 'rollup-plugin-svelte';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import sveltePreprocess from 'svelte-preprocess';
import typescript from '@rollup/plugin-typescript';
import { string } from 'rollup-plugin-string'
import postcss from 'rollup-plugin-postcss';
import replace from '@rollup/plugin-replace';
import alias from '@rollup/plugin-alias'
import tsConfig from './tsconfig.json'
import path from 'path'

const projectSrcDir = path.resolve(__dirname, 'src')

const tsConfigPaths = Object
  .entries(tsConfig.compilerOptions.paths)
  .flatMap(([find, replacements]) => {
    return {
      find: find.slice(0, -2),
      replacement: path.resolve(projectSrcDir, replacements[0].slice(0, -1)),
    }
  })


const production = !process.env.ROLLUP_WATCH;

function serve() {
  let server;

  function toExit() {
    if (server) server.kill(0);
  }

  return {
    writeBundle() {
      if (server) return;
      server = require('child_process').spawn('npm', ['run', 'start', '--', '--dev'], {
        stdio: ['ignore', 'inherit', 'inherit'],
        shell: true
      });

      process.on('SIGTERM', toExit);
      process.on('exit', toExit);
    }
  };
}

const staticFolder = process.env.STATIC_FOLDER || "../static"

export default {
  input: 'src/main.ts',
  output: {
    sourcemap: true,
    format: 'iife',
    name: 'app',
    file: path.join(staticFolder, 'bundle.js')
  },
  plugins: [
    alias({
      entries: [
        ...tsConfigPaths
      ]
    }),

    svelte({
      preprocess: sveltePreprocess({ sourceMap: !production }),
      compilerOptions: {
        // enable run-time checks when not in production
        dev: !production,
      }
    }),
    // we'll extract any component CSS out into
    // a separate file - better for performance
    // css({ output: 'global.css' }),
    postcss({}),

    replace({
      'process.env.NODE_ENV': JSON.stringify('production'),
      'id="wordcloud"': 'class="wordcloud"',
      '#wordcloud': '.wordcloud'
    }),


    // If you have external dependencies installed from
    // npm, you'll most likely need these plugins. In
    // some cases you'll need additional configuration -
    // consult the documentation for details:
    // https://github.com/rollup/plugins/tree/master/packages/commonjs
    resolve({
      browser: true,
      dedupe: ['svelte']
    }),
    string({
      include: 'src/components/img/**/*.svg',
    }),
    commonjs(),
    typescript({
      sourceMap: !production,
      inlineSources: !production
    }),

    // In dev mode, call `npm run start` once
    // the bundle has been generated
    !production && serve(),

    // Watch the `public` directory and refresh the
    // browser on changes when not in production
    !production && livereload('public'),

    // If we're building for production (npm run build
    // instead of npm run dev), minify
    production && terser()
  ],
  watch: {
    clearScreen: false
  }
};
