package main

import (
	"embed"
	"fmt"
	"log"
	"net/http"
	"os"
	"text/template"

	"gitlab.com/kiiluchris/web-page-word-comparison"
	"gopkg.in/yaml.v2"

	"github.com/kelseyhightower/envconfig"
)

var templates = template.Must(template.ParseFiles("./index.html"))

//go:embed static/*
var staticFS embed.FS

func UiHandler(w http.ResponseWriter, r *http.Request) {
	log.Println(templates.ExecuteTemplate(w, "index.html", nil))
}

type Config struct {
	Postgres struct {
		Host     string `yaml:"host" envconfig:"POSTGRES_HOST"`
		Db       string `yaml:"db" envconfig:"POSTGRES_DB"`
		User     string `yaml:"user" envconfig:"POSTGRES_USER"`
		Password string `yaml:"password" envconfig:"POSTGRES_PASSWORD"`
	} `yaml:"postgres"`
	FileStorageDirectory string `yaml:"file_storage_directory" envconfig:"FILE_STORAGE_DIRECTORY"`
	Port                 uint   `yaml:"port" envconfig:"PORT"`
}

func handleError(err error) {
	fmt.Println(err)
	os.Exit(2)
}

func readConfigFromFile(cfg *Config, filePath string) {
	file, err := os.Open(filePath)
	if err != nil {
		handleError(err)
	}
	err = yaml.NewDecoder(file).Decode(cfg)
	if err != nil {
		handleError(err)
	}
}

func readConfigFromEnv(cfg *Config) {
	err := envconfig.Process("", cfg)
	if err != nil {
		handleError(err)
	}
}

func main() {
	args := os.Args[1:]
	var cfg Config
	if len(args) > 1 {
		readConfigFromFile(&cfg, args[1])
	}
	readConfigFromEnv(&cfg)
	if cfg.Port == 0 {
		cfg.Port = 9999
	}
	server := word_comparison.NewHtmlWordCountServer(
		cfg.FileStorageDirectory,
		cfg.Postgres.Host,
		cfg.Postgres.Db,
		cfg.Postgres.User,
		cfg.Postgres.Password,
	)
	fileServer := http.FileServer(http.FS(staticFS))
	router := http.NewServeMux()
	router.Handle("/static/", http.StripPrefix("/", fileServer))
	router.Handle("/api/", server.Handler)
	router.HandleFunc("/", UiHandler)

	fmt.Printf("Listening on port %d", cfg.Port)
	log.Fatalln(http.ListenAndServe(fmt.Sprintf(":%d", cfg.Port), router))
}
