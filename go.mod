module gitlab.com/kiiluchris/md-engine-and-word-comparison-demo-app

// +heroku goVersion 1.16
go 1.16

require (
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/ziutek/mymysql v1.5.4 // indirect
	gitlab.com/kiiluchris/web-page-word-comparison v0.0.0-20220122135913-0b35bed6a917
	golang.org/x/net v0.0.0-20220121210141-e204ce36a2ba // indirect
	gopkg.in/yaml.v2 v2.4.0
)
