# MD Engine and Word Comparison Demo App

Application to created to interact with the
[simple-markdown-engine](https://gitlab.com/kiiluchris/simple-markdown-engine) and
[web-page-word-comparison](https://gitlab.com/kiiluchris/web-page-word-comparison)
repositories via a web page.

## Requirements

## PostgreSQL

Used to store cached web page metadata.

1. Create a PostgreSQL database that is set up to use SSL connections.

## Go

Go 1.16+ is required to build the application due to the use of
the embed feature to package
database migrations as part of the web-page-word-comparison library.

## Installation

### Install using go

Install the application using go 1.16+ using the following command

```bash
go install gitlab.com/kiiluchris/md-engine-and-word-comparison-demo-app@latest
```

This exposes the command `md-engine-and-word-comparison-demo-app`
which can be used from the terminal to launch the application.

### Download a binary from build artifacts

Click one of the following links to download a binary for your platform.
Note that the binaries are built for 64 bit systems.

- [Linux](https://gitlab.com/kiiluchris/md-engine-and-word-comparison-demo-app/-/jobs/2004501751/artifacts/raw/build/demo-app-linux-amd64?inline=false)
- [Mac OS](https://gitlab.com/kiiluchris/md-engine-and-word-comparison-demo-app/-/jobs/2004501751/artifacts/raw/build/demo-app-darwin-amd64?inline=false)
- [Windows](https://gitlab.com/kiiluchris/md-engine-and-word-comparison-demo-app/-/jobs/2004501751/artifacts/raw/build/demo-app-windows-amd64.exe?inline=false)

## Usage

Create a YAML configuration file with the PostgreSQL connection
details, the application port and the folder where files can
be cached.

```yaml
postgres:
  host: localhost
  db: postgres
  user: user
  password: password
file_storage_directory: /path/to/cache/files
port: 8888
```

Run the application using the installed command.
This command assumes the configuration file has been named
`config.yaml`.

If you performed installation using go run
```bash
md-engine-and-word-comparison-demo-app config.yaml
```

If you instead downloaded one of the binary executables run

```bash
# First for Linux or Mac OS
chmod +x demo-app-[platform]-amd64
# Run the application
./demo-app-[platform]-amd64 config.yaml
```


## Screenshots

### Markdown Engine

This is the first page seen after opening the application.
The text shown in the editor is shown as default and is editable.

![Markdown Engine UI](./screenshots/markdown-engine.png)

### Word Count Comparison

This page makes use of the API from the
[web-page-word-comparison](https://gitlab.com/kiiluchris/web-page-word-comparison)
repo and is accessed from the Word Count Occurrences link in the top
navigation bar.

![Word Occurrence Count](./screenshots/word-occurrence-ui.png)

